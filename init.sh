#!/bin/bash

clear
echo
echo "###############################################"
echo "#                   3MP-CLI                   #"
echo "###############################################"
echo

# Support functions
is_root() {
  if [ "$EUID" -ne 0 ]; then
    echo "[INFO] | Please run as root. Exiting..."
    exit
  fi
}

current_dir() {
  PWD=$(pwd)
}

check_os(){
. /etc/os-release

case $ID in
  debian)
    SERVER_OS="debian"
    OS_VER=$VERSION_ID
    ;;
  rocky)
    SERVER_OS="rocky"
    OS_VER=${VERSION_ID:0:1}
    ;;
  *)
    echo "[INFO] | WL-CLI only support Debian 11, Debian 12, Rocky Linux 8 and Rocky Linux 9..."
    echo "[INFO] | Exiting..."
    exit
    ;;
esac
}

check_panel() {
  echo "[INFO] | Checking your server..."
  echo
  if [ -d /usr/local/cpanel ]; then
    echo "[INFO] | cPanel is installed. Exiting..."
    exit
  fi
  if [ -d /usr/local/psa ]; then
    echo "[INFO] | Plesk is installed. Exiting..."
    exit
  fi
  if [ -d /usr/local/directadmin ]; then
    echo "[INFO] | DirectAdmin is installed. Exiting..."
  fi
  if [ -d /usr/local/CyberCP ]; then
    echo "[INFO] | CyberPanel is installed. Exiting..."
  fi
  if [ -d /home/cloudpanel ]; then
    echo "[INFO] | CloudPanel is installed. Exiting..."
  fi
  if [ -d /usr/local/wl ]; then
    echo "[INFO] | 3MP-CLI is installed. You can use '3mp' command to manage. Exiting..."
    exit
  fi
}

confirm_install() {
  read -p "Do you want to install 3MP-CLI? (Y/n): " INSTALL_CONFIRM
  if [[ "$INSTALL_CONFIRM" =~ ^[Yy] ]]; then
    init_wl
  else
    echo
    echo "[INFO] | Exiting..."
    echo
  fi
}

init_wl(){
  bash $PWD/os/${SERVER_OS}/${OS_VER}/deploy.sh
}

is_root
check_os
check_panel
confirm_install