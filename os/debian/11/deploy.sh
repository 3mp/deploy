#!/bin/bash

# Utils
export DEBIAN_FRONTEND=noninteractive
MYSQL_ROOT_PASSWORD=$(head /dev/urandom | tr -dc 'A-Za-z0-9' | head -c 16)

# Update packages
apt update
apt upgrade -y
apt autoremove -y

# Required packages
apt install -y curl gnupg2 ca-certificates lsb-release debian-archive-keyring

# Setup repo
## NGINX
### Signing key
curl https://nginx.org/keys/nginx_signing.key | gpg --dearmor \
    | sudo tee /usr/share/keyrings/nginx-archive-keyring.gpg >/dev/null

### Mainline repo
echo "deb [signed-by=/usr/share/keyrings/nginx-archive-keyring.gpg] \
http://nginx.org/packages/mainline/debian `lsb_release -cs` nginx" \
    | sudo tee /etc/apt/sources.list.d/nginx.list

### Prefer repo
echo -e "Package: *\nPin: origin nginx.org\nPin: release o=nginx\nPin-Priority: 900\n" \
    | sudo tee /etc/apt/preferences.d/99nginx

## MariaDB
curl -LsS https://r.mariadb.com/downloads/mariadb_repo_setup | bash

## PHP
### Signing key
curl -sSLo /usr/share/keyrings/deb.sury.org-php.gpg https://packages.sury.org/php/apt.gpg

### Repo
sh -c 'echo "deb [signed-by=/usr/share/keyrings/deb.sury.org-php.gpg] https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'

## Update apt
apt update

# LEMP
## NGINX
apt install -y nginx

## MariaDB
apt install -y mariadb-server mariadb-client

## PHP
### PHP 8.2
# apt install -y php81 php81-{fpm,bcmath,mysqlnd,common,cli,gmp,gd,mbstring,xml,imap,curl,opcache,zip,apcu,redis,intl,imagick}

### PHP 8.1
apt install -y php8.1 php8.1-{fpm,bcmath,mysqlnd,common,cli,gmp,gd,mbstring,xml,imap,curl,opcache,zip,apcu,redis,intl,imagick}

### PHP 8.0

### PHP 7.4
# apt install -y php81 php81-{fpm,bcmath,mysqlnd,common,cli,gmp,gd,mbstring,xml,imap,curl,opcache,zip,apcu,redis,intl,imagick}

## Utils
apt install -y unzip wget certbot python3-certbot-nginx

# Setting up services
systemctl enable nginx.service
systemctl enable --now mariadb.service
# systemctl enable php8.2-fpm.service
systemctl enable php8.1-fpm.service
# systemctl enable php7.4-fpm.service

# Setting up configuration
## MariaDB
### Create MariaDB init.sql
cat << EOF > /tmp/init.sql
UPDATE mysql.global_priv SET priv=json_set(priv, '\$.password_last_changed', UNIX_TIMESTAMP(), '\$.plugin', 'mysql_native_password', '\$.authentication_string', 'invalid', '\$.auth_or', json_array(json_object(), json_object('plugin', 'unix_socket'))) WHERE User='root';
UPDATE mysql.global_priv SET priv=json_set(priv, '\$.plugin', 'mysql_native_password', '\$.authentication_string', PASSWORD('$MYSQL_ROOT_PASSWORD')) WHERE User='root';
DELETE FROM mysql.global_priv WHERE User='';
DELETE FROM mysql.global_priv WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
DROP DATABASE IF EXISTS test;
DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
FLUSH PRIVILEGES;
EOF

### MariaDB init
mariadb -sfu root < /tmp/init.sql
rm -f /tmp/init.sql

## Certbot
### Certbot register
certbot register -m gg@gm.com --agree-tos --no-eff-email

### Auto renew
crontab -l | { cat; echo "45 2 * * * /usr/bin/certbot renew --quiet"; } | crontab -

# CLI manage tool


# Output

echo "Done"
echo
echo $MYSQL_ROOT_PASSWORD